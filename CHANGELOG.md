# CHANGELOG 


## version:0.1.29-commit:2023/3/5  9:6 
### fix 

添加CICD中对命令行的hook脚本示例



## version:0.1.28-commit:2023/3/4  16:6 
### fix 

修复了之前版本的小问题



## version:0.1.25-commit:2023/3/4  14:28 
### docs 

添加运行时对用户的说明



## version:0.1.22-commit:2023/3/4  14:12 
### fix 

解决引用fs的bug



## version:0.1.19-commit:2023/3/4  12:57 
### 修复用户安装时需要手动添加依赖的bug 

undefined






## version:0.1.13-commit:2023/3/3  20:23 
### fix 

解决生成readme文件时候package.json校验的bug



## version:0.1.12-commit:2023/3/3  19:56 
### feat 

更改cicd中readmeupdate的运行规则



## version:0.1.9-commit:2023/3/3  16:40 
### feat 

调试完生成的文件和结果



## version:0.1.7-commit:2023/3/3  16:25 
### fix 

修复全部居中的bug



## version:0.1.6-commit:2023/3/3  15:14 
### refactor 

删掉测试文件夹



## version:0.1.5-commit:2023/3/3  15:11 
### feat 

添加readme自动生成功能,优化本身readme







  